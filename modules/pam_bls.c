/*!\file
 * This file contains the BLS PAM module. Only the user authentication service
 * is implemented. See the
 * <a href="http://www.linux-pam.org/Linux-PAM-html/Linux-PAM_MWG.html">The Linux-PAM Module Writers' Guide</a>
 * for more details.
 *
 * \copyright 2017 Lely Industries N.V.
 *
 * \author J. S. Seldenthuis <jseldenthuis@lely.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <syslog.h>
#include <unistd.h>

#define PAM_SM_AUTH
#ifdef HAVE_SECURITY_PAM_EXT_H
#include <security/pam_ext.h>
#endif
#ifdef HAVE_SECURITY_PAM_MODULES_H
#include <security/pam_modules.h>
#endif

#ifndef BLS_MAXSIG
/*!
 * The maximum size (in characters, excluding the terminating null byte) of a
 * Base32-encoded BLS signature.
 */
#define BLS_MAXSIG	255
#endif

#ifndef BLS_MSGFMT
#error BLS PAM message location not defined.
#endif

#ifndef BLS_PAMCHK
#error bls-pamchk helper binary not defined.
#endif

int
pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	(void)argc; (void)argv;

	int retval;
	int errsv = errno;

	const char *user = NULL;
	retval = pam_get_user(pamh, &user, NULL);
	if (retval != PAM_SUCCESS)
		return retval;
	if (!user)
		return PAM_USER_UNKNOWN;

	// Construct the location of the message file for the user.
	char msgfile[PATH_MAX];
	retval = snprintf(msgfile, sizeof(msgfile), BLS_MSGFMT, user);
	if (retval < 0 || (size_t)retval >= sizeof(msgfile)) {
		pam_syslog(pamh, LOG_ERR, "user name too long");
		return PAM_BUF_ERR;
	}

	// Check if the file exists and we have read access.
	if (access(msgfile, R_OK) == -1) {
		switch (errno) {
		case EACCES:
			errno = errsv;
			return PAM_PERM_DENIED;
		case ENOENT:
			errno = errsv;
			return PAM_USER_UNKNOWN;
		default:
			pam_syslog(pamh, LOG_ERR, "%s: %s", msgfile,
					strerror(errno));
			return PAM_SYSTEM_ERR;
		}
	}

	// Print the first line from the message file, unless the PAM_SILENT
	// flag is set.
	if (!(flags & PAM_SILENT)) {
		FILE *stream = fopen(msgfile, "r");
		if (!stream) {
			pam_syslog(pamh, LOG_ERR, "%s: %s", msgfile,
					strerror(errno));
			retval = PAM_SYSTEM_ERR;
			goto error_fopen;
		}

		// Read the first line from the file.
		char *line = NULL;
		size_t n = 0;
		if (getline(&line, &n, stream) == -1) {
			pam_syslog(pamh, LOG_ERR, "%s: %s", msgfile,
					strerror(errno));
			retval = PAM_SYSTEM_ERR;
			goto error_getline;
		}

		// Remove trailing whitespace.
		n = strlen(line);
		while (n && isspace((unsigned char)line[n - 1]))
			line[--n] = '\0';

		char *response = NULL;
		retval = pam_prompt(pamh, PAM_TEXT_INFO, &response, "%s", line);
		free(response);

	error_getline:
		free(line);
		fclose(stream);
	error_fopen:
		if (retval != PAM_SUCCESS)
			return retval;
	}

	// Obtain the signature.
	char *response = NULL;
	retval = pam_prompt(pamh, PAM_PROMPT_ECHO_ON, &response, "Signature: ");
	if (retval != PAM_SUCCESS)
		return retval;
	if (!response)
		return PAM_AUTHTOK_ERR;

	// Move the signature to the stack.
	char sig[BLS_MAXSIG + 1];
	size_t n = strlen(response);
	if (n < sizeof(sig))
		strcpy(sig, response);
	memset(response, 0, n);
	free(response);
	if (n >= sizeof(sig)) {
		pam_syslog(pamh, LOG_ERR, "signature too long");
		return PAM_BUF_ERR;
	}

	// Create a pipe to communicate to bls-pamchk.
	int fds[2];
	if (pipe(fds) == -1) {
		pam_syslog(pamh, LOG_CRIT, "unable to open pipe: %s",
				strerror(errno));
		memset(sig, 0, sizeof(sig));
		return PAM_SYSTEM_ERR;
	}

	// Create a child process in which to execute bls-pamchk.
	pid_t pid = fork();
	if (pid == -1) {
		pam_syslog(pamh, LOG_CRIT, "unable to fork: %s",
				strerror(errno));
		close(fds[1]);
		close(fds[0]);
		memset(sig, 0, sizeof(sig));
		return PAM_SYSTEM_ERR;
	}

	if (!pid) {
		memset(sig, 0, sizeof(sig));

		// Close the write end of the pipe, since we don't need it.
		close(fds[1]);

		// Map the read end of the pipe to stdin.
		if (dup2(fds[0], STDIN_FILENO) == -1) {
			pam_syslog(pamh, LOG_CRIT,
					"unable to duplicate stdin: %s",
					strerror(errno));
			close(fds[0]);
			_Exit(PAM_SYSTEM_ERR);
		}
		close(fds[0]);

		// Execute bls-pamchk.
		char *const argv[] = { BLS_PAMCHK, msgfile, NULL };
		execv(argv[0], argv);
		pam_syslog(pamh, LOG_ERR, "%s: %s", BLS_PAMCHK,
				strerror(errno));
		_Exit(PAM_SYSTEM_ERR);
	}

	// Close the read end of the pipe, since we don't need it.
	close(fds[0]);

	// Write the signature to bls-pamchk.
	const char *cp = sig;
	while (*cp) {
		ssize_t n;
		do n = write(fds[1], cp, strlen(cp));
		while (n == -1 && errno == EINTR);
		if (n == -1) {
			pam_syslog(pamh, LOG_CRIT,
					"unable to write signature: %s",
					strerror(errno));
			retval = PAM_SYSTEM_ERR;
			break;
		}
		cp += n;
		errno = errsv;
	}

	// Close the write end of the pipe to signal bls-pamchk to stop reading.
	close(fds[1]);

	memset(sig, 0, sizeof(sig));

	// Wait for bls-pamchk to exit.
	int stat_val = 0;
	do {
		pid_t wpid;
		do wpid = waitpid(pid, &stat_val, 0);
		while (wpid == -1 && errno == EINTR);
		if (wpid == -1) {
			pam_syslog(pamh, LOG_ERR, "%s: %s", BLS_PAMCHK,
					strerror(errno));
			retval = PAM_SYSTEM_ERR;
			break;
		}
		errno = errsv;
	} while (!WIFEXITED(stat_val) && !WIFSIGNALED(stat_val));

	// Obtain the return value from bls-pamchk, if available, and if no
	// other error occurred.
	if (WIFEXITED(stat_val)) {
		if (retval == PAM_SUCCESS)
			retval = WEXITSTATUS(stat_val);
	} else {
		pam_syslog(pamh, LOG_ERR, "%s: abnormal termination",
				BLS_PAMCHK);
		retval = PAM_SYSTEM_ERR;
	}

	return retval;
}

int
pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	(void)pamh; (void)flags; (void)argc; (void)argv;

	return PAM_IGNORE;
}

