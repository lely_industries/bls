/*!\file
 * This file contains the tool used to verify a BLS signature. See the
 * <a href="https://crypto.stanford.edu/pbc/manual/ch02s01.html">BLS signatures</a>
 * tutorial in the PBC Library Manual for details.
 *
 * \copyright 2017 Lely Industries N.V.
 *
 * \author J. S. Seldenthuis <jseldenthuis@lely.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tools.h"

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define HELP \
	"Usage: %s [options...] <parameter file> <public key file>\n" \
	"       <message file>\n" \
	"Options:\n" \
	"  -b, --binary          Read binary signature\n" \
	"  -h, --help            Display this information\n" \
	"  -i <file>, --input=<file>\n" \
	"                        Read the signature from <file> instead of stdin\n" \
	"  -q, --quiet           Do not print errors\n"

#define FLAG_BINARY	0x01
#define FLAG_HELP	0x02
#define FLAG_QUIET	0x04

int
main(int argc, char *argv[])
{
	const char *cmd = cmdname(argv[0]);

	int flags = 0;
	const char *infile = NULL;
	const char *parfile = NULL;
	const char *pubfile = NULL;
	const char *msgfile = NULL;

	opterr = 0;
	optind = 1;
	int optpos = 0;
	while (optind < argc) {
		char *arg = argv[optind];
		if (*arg != '-') {
			optind++;
			switch (optpos++) {
			case 0:
				parfile = arg;
				break;
			case 1:
				pubfile = arg;
				break;
			case 2:
				msgfile = arg;
				break;
			default:
				fprintf(stderr, "%s: extra argument %s\n", cmd,
						arg);
				break;
			}
		} else if (*++arg == '-') {
			optind++;
			if (!*++arg)
				break;
			if (!strcmp(arg, "binary")) {
				flags |= FLAG_BINARY;
			} else if (!strcmp(arg, "help")) {
				flags |= FLAG_HELP;
			} else if (!strncmp(arg, "input=", 6)) {
				infile = arg + 6;
			} else if (!strcmp(arg, "quiet")) {
				flags |= FLAG_QUIET;
			} else {
				fprintf(stderr, "%s: illegal option -- %s\n",
						cmd, arg);
			}
		} else {
			int c = getopt(argc, argv, ":bhi:q");
			if (c == -1)
				break;
			switch (c) {
			case ':':
				fprintf(stderr, "%s: option requires an argument -- %c\n",
						cmd, optopt);
				break;
			case '?':
				fprintf(stderr, "%s: illegal option -- %c\n",
						cmd, optopt);
				break;
			case 'b':
				flags |= FLAG_BINARY;
				break;
			case 'h':
				flags |= FLAG_HELP;
				break;
			case 'i':
				infile = optarg;
				break;
			case 'q':
				flags |= FLAG_QUIET;
				break;
			}
		}
	}
	for (char *arg = argv[optind]; optind < argc; arg = argv[++optind]) {
		switch (optpos++) {
		case 0:
			parfile = arg;
			break;
		case 1:
			pubfile = arg;
			break;
		case 2:
			msgfile = arg;
			break;
		default:
			fprintf(stderr, "%s: extra argument %s\n", cmd, arg);
			break;
		}
	}

	if (flags & FLAG_HELP) {
		fprintf(stderr, HELP, cmd);
		return EXIT_SUCCESS;
	}

	if (optpos < 1 || !parfile) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: no parameter file specified\n",
					cmd);
		return EXIT_FAILURE;
	}

	if (optpos < 2 || !pubfile) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: no public key file specified\n",
					cmd);
		return EXIT_FAILURE;
	}

	if (optpos < 3 || !msgfile) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: no message file specified\n", cmd);
		return EXIT_FAILURE;
	}

	int retval = EXIT_FAILURE;
	gcry_error_t err;
	FILE *stream;
	size_t n;
	char *cp;
	int len, i;

	// Initialize Libgcrypt.
	if (!gcry_check_version(GCRYPT_VERSION)) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: libgcrypt version mismatch\n",
					cmd);
		goto error_gcry_check_version;
	}
	err = gcry_control(GCRYCTL_INIT_SECMEM, BLS_BUFSIZE, 0);
	assert(!err);
	err = gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
	assert(!err);

	// Allocate a secure memory buffer for reading files.
	char *buf = gcry_malloc_secure(BLS_BUFSIZE);
	if (!buf) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to allocate secure memory\n",
					cmd);
		goto error_malloc_buf;
	}

	pairing_t pairing;
	// Read the pairing parameters from file.
	stream = fopen(parfile, "r");
	if (!stream) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: %s\n", parfile, strerror(errno));
		goto error_fopen_parfile;
	}
	n = fread(buf, 1, BLS_BUFSIZE - 1, stream);
	buf[n] = '\0';
	if (ferror(stream) || !feof(stream)) {
		fclose(stream);
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to read entire file\n",
					parfile);
		goto error_fread_parfile;
	}
	fclose(stream);
	// Parse the pairing parameters.
	if (pairing_init_set_buf(pairing, buf, n)) {
		fprintf(stderr, "%s: unable to parse pairing parameters\n",
				parfile);
		goto error_init_pairing;
	}

	// Initialize the system parameters.
	element_t g;
	element_init_G2(g, pairing);

	// Initialize the public key.
	element_t public_key;
	element_init_G2(public_key, pairing);

	// Read the public key from file.
	stream = fopen(pubfile, "r");
	if (!stream) {
		fprintf(stderr, "%s: %s\n", pubfile, strerror(errno));
		goto error_fopen_pubfile;
	}
	n = fread(buf, 1, BLS_BUFSIZE - 1, stream);
	buf[n] = '\0';
	if (ferror(stream) || !feof(stream)) {
		fclose(stream);
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to read entire file\n",
					pubfile);
		goto error_fread_pubfile;
	}
	fclose(stream);
	cp = buf;
	// Parse the system parameters.
	if (!(len = element_set_str(g, cp, 10))) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to parse system parameters\n",
					pubfile);
		goto error_set_g;
	}
	cp += len;
	// Parse the public key.
	if (!(len = element_set_str(public_key, cp, 10))) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to parse public key\n",
					pubfile);
		goto error_set_public_key;
	}

	// Initialize the hashing context.
	gcry_md_hd_t hd;
	err = gcry_md_open(&hd, BLS_MD_ALGO, 0);
	if (err) {
		errno = gcry_err_code_to_errno(gcry_err_code(err));
		if (errno && !(flags & FLAG_QUIET))
			fprintf(stderr, "%s: %s\n", cmd, strerror(errno));
		goto error_open_hd;
	}

	// Read the message from file and compute the hash.
	stream = fopen(msgfile, "rb");
	if (!stream) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: %s\n", msgfile, strerror(errno));
		goto error_fopen_msgfile;
	}
	while (!feof(stream)) {
		n = fread(buf, 1, BLS_BUFSIZE, stream);
		if (n) {
			gcry_md_write(hd, buf, n);
		} else if (ferror(stream)) {
			fclose(stream);
			if (!(flags & FLAG_QUIET))
				fprintf(stderr, "%s: unable to read entire file\n",
						msgfile);
			goto error_fread_msgfile;
		}
	}
	fclose(stream);

	// Generate the hash element.
	element_t h;
	element_init_G1(h, pairing);
	element_from_hash(h, gcry_md_read(hd, BLS_MD_ALGO),
			gcry_md_get_algo_dlen(BLS_MD_ALGO));

	// Allocate a storage buffer for the signature.
	len = pairing_length_in_bytes_x_only_G1(pairing);
	assert(len > 0);
	unsigned char *data = malloc(len);
	if (!data) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: %s\n", cmd, strerror(errno));
		goto error_malloc_data;
	}
	memset(data, 0, len);

	stream = stdin;
	if (infile) {
		stream = fopen(infile, (flags & FLAG_BINARY) ? "rb" : "r");
		if (!stream) {
			if (!(flags & FLAG_QUIET))
				fprintf(stderr, "%s: %s\n", infile,
						strerror(errno));
			goto error_fopen_infile;
		}
	}

	if (flags & FLAG_BINARY) {
		// Read the binary signature.
		if (fread(data, 1, len, stream) != (size_t)len) {
			if (!(flags & FLAG_QUIET))
				fprintf(stderr, "%s: unable to read signature\n",
						infile ? infile : "<stdin>");
			if (infile)
				fclose(stream);
			goto error_fread_infile;
		}
	} else {
		// Read the signature.
		n = fread(buf, 1, BLS_BUFSIZE - 1, stream);
		buf[n] = '\0';
		if (ferror(stream) || !feof(stream)) {
			if (!(flags & FLAG_QUIET))
				fprintf(stderr, "%s: unable to read signature\n",
						infile ? infile : "<stdin>");
			if (infile)
				fclose(stream);
			goto error_fread_infile;
		}
		cp = buf;

		// Parse the Base32 signature (see RFC 4648).
		unsigned char *bp = data;
		i = 0;
		while (*cp && bp < data + len) {
			// Obtain the next 5 bits and ignore unknown characters.
			char c = *cp++;
			unsigned char b;
			if (isalpha((unsigned char)c))
				b = toupper((unsigned char)c) - 'A';
			else if (c >= '2' && c <= '7')
				b = 26 + c - '2';
			else
				continue;
			// Add the bits to the storage buffer.
			if (i <= 3) {
				*bp |= (b << (3 - i));
				if ((i += 5) == 8) {
					bp++;
					i = 0;
				}
			} else {
				*bp++ |= b >> (i - 3);
				if (bp < data + len)
					*bp |= b << (8 - (i -= 3));
			}
		}
	}

	if (infile)
		fclose(stream);

	// Avoid a division-by-zero error.
	for (i = 0; i < len && !data[i]; i++);
	if (i == len) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: invalid signature\n", cmd);
		goto error_data;
	}

	// Obtain the x-coordinate of the signature.
	element_t sig;
	element_init_G1(sig, pairing);
	element_from_bytes_x_only(sig, data);

	// Apply the pairing to the signature and system parameters.
	element_t temp1;
	element_init_GT(temp1, pairing);
	pairing_apply(temp1, sig, g, pairing);

	// Apply the pairing to the hash and public key.
	element_t temp2;
	element_init_GT(temp2, pairing);
	pairing_apply(temp2, h, public_key, pairing);

	// Compare the pairing outputs. We might have to try twice, since we
	// only have the x-coordinate of the signature. See the Import/export
	// tutorial in the PBC Library Manual for details.
	if (element_cmp(temp1, temp2)) {
		element_invert(temp1, temp1);
		if (element_cmp(temp1, temp2)) {
			if (!(flags & FLAG_QUIET))
				fprintf(stderr, "%s: signature check failed\n",
						cmd);
			goto error_sig;
		}
	}

	retval = EXIT_SUCCESS;
error_sig:
	element_clear(temp2);
	element_clear(temp1);
	// Overwrite the signature with random data.
	element_random(sig);
	element_clear(sig);
error_data:
error_fread_infile:
error_fopen_infile:
	// Clear the storage buffer for the signature.
	memset(data, 0, len);
	free(data);
error_malloc_data:
	element_clear(h);
error_fread_msgfile:
error_fopen_msgfile:
	gcry_md_close(hd);
error_open_hd:
error_set_public_key:
error_set_g:
error_fread_pubfile:
error_fopen_pubfile:
	element_clear(public_key);
	element_clear(g);
	pairing_clear(pairing);
error_init_pairing:
error_fread_parfile:
error_fopen_parfile:
	gcry_free(buf);
error_malloc_buf:
	gcry_control(GCRYCTL_TERM_SECMEM);
error_gcry_check_version:
	return retval;
}

