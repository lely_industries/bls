/*!\file
 * This file contains the tool used to generate a public/private key pair for
 * signing/verifying BLS signatures. See the
 * <a href="https://crypto.stanford.edu/pbc/manual/ch02s01.html">BLS signatures</a>
 * tutorial in the PBC Library Manual for details.
 *
 * \copyright 2017 Lely Industries N.V.
 *
 * \author J. S. Seldenthuis <jseldenthuis@lely.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tools.h"

#include <assert.h>
#include <errno.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <gpgme.h>

#ifndef BLS_PARFILE
#define BLS system parameters file not defined.
#endif

#ifndef BLS_KEYFILE
#define BLS private key file not defined.
#endif

#ifndef BLS_GPGFILE
#define BLS encrypted private key file not defined.
#endif

#ifndef BLS_PUBFILE
#define BLS public key file not defined.
#endif

#define HELP \
	"Usage: %s [options...]\n" \
	"Options:\n" \
	"  -b <n>, --bits=<n>    Use <n> bits (default: 160)\n" \
	"  -h, --help            Display this information\n" \
	"  -p, --password        Encrypt the private key with a password\n" \
	"  -r <file>, --random=<file>\n" \
	"                        Use <file> as a source of random bytes\n" \
	"                        (default: /dev/urandom)\n"

#define FLAG_HELP	0x01
#define FLAG_PASSWORD	0x02

int
main(int argc, char *argv[])
{
	const char *cmd = cmdname(argv[0]);

	int flags = 0;
	int bits = BLS_BITS;
	char *randfile = NULL;

	opterr = 0;
	optind = 1;
	while (optind < argc) {
		char *arg = argv[optind];
		if (*arg != '-') {
			optind++;
			fprintf(stderr, "%s: extra argument %s\n", cmd, arg);
		} else if (*++arg == '-') {
			optind++;
			if (!*++arg)
				break;
			if (!strncmp(arg, "bits=", 5)) {
				bits = atoi(arg + 5);
			} else if (!strcmp(arg, "help")) {
				flags |= FLAG_HELP;
			} else if (!strcmp(arg, "password")) {
				flags |= FLAG_PASSWORD;
			} else if (!strncmp(arg, "random=", 7)) {
				randfile = arg + 7;
			} else {
				fprintf(stderr, "%s: illegal option -- %s\n",
						cmd, arg);
			}
		} else {
			int c = getopt(argc, argv, ":b:hpr:");
			if (c == -1)
				break;
			switch (c) {
			case ':':
				fprintf(stderr, "%s: option requires an argument -- %c\n",
						cmd, optopt);
				break;
			case '?':
				fprintf(stderr, "%s: illegal option -- %c\n",
						cmd, optopt);
				break;
			case 'b':
				bits = atoi(optarg);
				break;
			case 'h':
				flags |= FLAG_HELP;
				break;
			case 'p':
				flags |= FLAG_PASSWORD;
				break;
			case 'r':
				randfile = optarg;
				break;
			}
		}
	}

	if (flags & FLAG_HELP) {
		fprintf(stderr, HELP, cmd);
		return EXIT_SUCCESS;
	}

	int retval = EXIT_FAILURE;
	gpg_error_t err;
	FILE *stream;

	// Initialize GPGME, if necessary.
	if (flags & FLAG_PASSWORD) {
		setlocale(LC_ALL, "");
		if (!gpgme_check_version(NULL)) {
			fprintf(stderr, "%s: GPGME version mismatch\n", cmd);
			goto error_gpgme_check_version;
		}
		err = gpgme_set_locale(NULL, LC_CTYPE,
				setlocale(LC_CTYPE, NULL));
		assert(!err);
		err = gpgme_set_locale(NULL, LC_MESSAGES,
				setlocale(LC_MESSAGES, NULL));
		assert(!err);
		err = gpgme_engine_check_version(GPGME_PROTOCOL_OpenPGP);
		assert(!err);
	}

	if (randfile)
		pbc_random_set_file(randfile);

	// Generate type F pairing parameters.
	pbc_param_t fp;
	pbc_param_init_f_gen(fp, bits);

	// Write the pairing parameters to file.
	stream = fopen(BLS_PARFILE, "w");
	if (!stream) {
		fprintf(stderr, "%s: %s\n", BLS_PARFILE, strerror(errno));
		goto error_fopen_BLS_PARFILE;
	}
	pbc_param_out_str(stream, fp);
	fclose(stream);
	// Set file mode to rw- r-- r--.
	if (chmod(BLS_PARFILE, 0644) == -1)
		fprintf(stderr, "%s: %s\n", BLS_PARFILE, strerror(errno));

	// Initialize the pairing.
	pairing_t pairing;
	pairing_init_pbc_param(pairing, fp);

	// Generate system parameters.
	element_t g;
	element_init_G2(g, pairing);
	element_random(g);

	// Generate a private key.
	element_t secret_key;
	element_init_Zr(secret_key, pairing);
	element_random(secret_key);

	// Write the private key to file.
	const char *keyfile = (flags & FLAG_PASSWORD)
			? BLS_GPGFILE : BLS_KEYFILE;
	stream = fopen(keyfile, "w");
	if (!stream) {
		fprintf(stderr, "%s: %s\n", keyfile, strerror(errno));
		goto error_fopen_keyfile;
	}
	if (flags & FLAG_PASSWORD) {
		// Print the private key to a temporary buffer on the stack.
		int len = element_snprintf(NULL, 0, "%B\n%B\n", g, secret_key);
		assert(len >= 0);
		char buf[len + 1];
		element_snprintf(buf, sizeof(buf), "%B\n%B\n", g, secret_key);

		gpgme_data_t in;
		err = gpgme_data_new_from_mem(&in, buf, strlen(buf), 1);
		// Overwrite the private key.
		memset(buf, 0, strlen(buf));
		if (err)
			goto error_new_in;

		// Create an OpenPGP context.
		gpgme_ctx_t ctx;
		err = gpgme_new(&ctx);
		if (err)
			goto error_new_ctx;
		err = gpgme_set_protocol(ctx, GPGME_PROTOCOL_OpenPGP);
		if (err)
			goto error_gpgme_set_protocol;
		gpgme_set_armor(ctx, 1);

		gpgme_data_t out;
		err = gpgme_data_new_from_stream(&out, stream);
		if (err)
			goto error_new_out;

		err = gpgme_op_encrypt(ctx, NULL, 0, in, out);

	error_gpgme_set_protocol:
		gpgme_data_release(out);
	error_new_out:
		gpgme_release(ctx);
	error_new_ctx:
		gpgme_data_release(in);
	error_new_in:
		fclose(stream);
		if (err) {
			errno = gpgme_err_code_to_errno(gpgme_err_code(err));
			if (errno)
				fprintf(stderr, "%s: %s\n", cmd,
						strerror(errno));
			goto error_encrypt;
		}
	} else {
		element_fprintf(stream, "%B\n%B\n", g, secret_key);
		fclose(stream);
	}
	// Set file mode to rw- --- ---.
	if (chmod(keyfile, 0600) == -1)
		fprintf(stderr, "%s: %s\n", keyfile, strerror(errno));

	// Generate the corresponding public key.
	element_t public_key;
	element_init_G2(public_key, pairing);
	element_pow_zn(public_key, g, secret_key);

	// Write the public key to file.
	stream = fopen(BLS_PUBFILE, "w");
	if (!stream) {
		fprintf(stderr, "%s: %s\n", BLS_PUBFILE, strerror(errno));
		goto error_fopen_BLS_PUBFILE;
	}
	element_fprintf(stream, "%B\n%B\n", g, public_key);
	fclose(stream);
	// Set file mode to rw- r-- r--.
	if (chmod(BLS_PUBFILE, 0644) == -1)
		fprintf(stderr, "%s: %s\n", BLS_PUBFILE, strerror(errno));

	retval = EXIT_SUCCESS;
	if (retval != EXIT_SUCCESS)
		remove(BLS_PUBFILE);
error_fopen_BLS_PUBFILE:
	element_clear(public_key);
error_encrypt:
	if (retval != EXIT_SUCCESS)
		remove(keyfile);
error_fopen_keyfile:
	// Overwrite the secret key with random data.
	element_random(secret_key);
	element_clear(secret_key);
	element_clear(g);
	pairing_clear(pairing);
	if (retval != EXIT_SUCCESS)
		remove(BLS_PARFILE);
error_fopen_BLS_PARFILE:
	pbc_param_clear(fp);
error_gpgme_check_version:
	return retval;
}

