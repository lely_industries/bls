/*!\file
 * This is the internal header file of the BLS command-line tools.
 *
 * \copyright 2017 Lely Industries N.V.
 *
 * \author J. S. Seldenthuis <jseldenthuis@lely.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LELY_BLS_TOOLS_TOOLS_H
#define LELY_BLS_TOOLS_TOOLS_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_GCRYPT_H
#include <gcrypt.h>
#else
#error Libgcrypt not found.
#endif

#ifdef HAVE_PBC_PBC_H
#include <pbc/pbc.h>
#else
#error PBC Library not found.
#endif

#ifndef BLS_BITS
//! The number of bits used when generating type F pairing parameters.
#define BLS_BITS	160
#endif

#ifndef BLS_BUFSIZE
//! The size of the secure memory buffer.
#define BLS_BUFSIZE	4096
#endif

#ifndef BLS_MD_ALGO
/*!
 * Use the SHA-224 hash when signing messages. Using a longer hash provides no
 * benefit, since element_from_hash() only uses the first #BLS_BITS bits to
 * generate an x-coordinate.
 */
#define BLS_MD_ALGO	GCRY_MD_SHA224
#endif

/*!
 * Extracts the command name from a path. This function returns a pointer to the
 * first character after the last separator ('/' or '\\') in \a path.
 */
static inline const char *cmdname(const char *path);

static inline const char *
cmdname(const char *path)
{
	const char *cmd = path;
	while (*cmd)
		cmd++;
#ifdef _WIN32
	while (cmd >= path && *cmd != '\\')
#else
	while (cmd >= path && *cmd != '/')
#endif
		cmd--;
	return ++cmd;
}

#endif

