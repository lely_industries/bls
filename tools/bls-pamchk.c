/*!\file
 * This file contains the tool used by the BLS PAM module to verify a signature.
 * See the
 * <a href="https://crypto.stanford.edu/pbc/manual/ch02s01.html">BLS signatures</a>
 * tutorial in the PBC Library Manual for details.
 *
 * \copyright 2017 Lely Industries N.V.
 *
 * \author J. S. Seldenthuis <jseldenthuis@lely.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tools.h"

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#ifdef HAVE_SECURITY__PAM_TYPES_H
#include <security/_pam_types.h>
#endif

#ifndef BLS_PAM_PARFILE
#error BLS PAM system parameters file undefined.
#endif

#ifndef BLS_PAM_PUBFILE
#error BLS PAM public key file undefined.
#endif

int
main(int argc, char *argv[])
{
	// Ignore termination signals.
	struct sigaction act = { .sa_handler = SIG_IGN };
	sigaction(SIGHUP, &act, NULL);
	sigaction(SIGINT, &act, NULL);
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGQUIT, &act, NULL);

	const char *cmd = cmdname(argv[0]);
	openlog(cmd, LOG_PID | LOG_CONS, LOG_AUTHPRIV);

	// Discourage accidental misuse.
	if (isatty(STDIN_FILENO) || argc != 2) {
		syslog(LOG_NOTICE, "inappropriate use of %s [UID=%d]", cmd,
				getuid());
		closelog();
		fprintf(stderr, "%s is not meant to be run from the command line\n",
				cmd);
		sleep(3);
		return PAM_SYSTEM_ERR;
	}
	const char *msgfile = argv[1];

	int retval = PAM_AUTHINFO_UNAVAIL;
	gcry_error_t err;
	FILE *stream;
	size_t n;
	char *cp;
	int len, i;

	// Initialize Libgcrypt.
	if (!gcry_check_version(GCRYPT_VERSION)) {
		syslog(LOG_CRIT, "libgcrypt version mismatch");
		retval = PAM_SYSTEM_ERR;
		goto error_gcry_check_version;
	}
	err = gcry_control(GCRYCTL_INIT_SECMEM, BLS_BUFSIZE, 0);
	assert(!err);
	err = gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
	assert(!err);

	// Allocate a secure memory buffer for reading files.
	char *buf = gcry_malloc_secure(BLS_BUFSIZE);
	if (!buf) {
		syslog(LOG_CRIT, "unable to allocate secure memory");
		retval = PAM_SYSTEM_ERR;
		goto error_malloc_buf;
	}

	pairing_t pairing;
	// Read the pairing parameters from file.
	stream = fopen(BLS_PAM_PARFILE, "r");
	if (!stream) {
		syslog(LOG_ALERT, "%s: %s", BLS_PAM_PARFILE, strerror(errno));
		goto error_fopen_BLS_PAM_PARFILE;
	}
	n = fread(buf, 1, BLS_BUFSIZE - 1, stream);
	buf[n] = '\0';
	if (ferror(stream) || !feof(stream)) {
		fclose(stream);
		syslog(LOG_ALERT, "%s: unable to read entire file",
				BLS_PAM_PARFILE);
		goto error_fread_BLS_PAM_PARFILE;
	}
	fclose(stream);
	// Parse the pairing parameters.
	if (pairing_init_set_buf(pairing, buf, n)) {
		syslog(LOG_ALERT, "%s: unable to parse pairing parameters",
				BLS_PAM_PARFILE);
		goto error_init_pairing;
	}

	// Initialize the system parameters.
	element_t g;
	element_init_G2(g, pairing);

	// Initialize the public key.
	element_t public_key;
	element_init_G2(public_key, pairing);

	// Read the public key from file.
	stream = fopen(BLS_PAM_PUBFILE, "r");
	if (!stream) {
		syslog(LOG_ALERT, "%s: %s", BLS_PAM_PUBFILE, strerror(errno));
		goto error_fopen_BLS_PAM_PUBFILE;
	}
	n = fread(buf, 1, BLS_BUFSIZE - 1, stream);
	buf[n] = '\0';
	if (ferror(stream) || !feof(stream)) {
		fclose(stream);
		syslog(LOG_ALERT, "%s: unable to read entire file",
				BLS_PAM_PUBFILE);
		goto error_fread_BLS_PAM_PUBFILE;
	}
	fclose(stream);
	cp = buf;
	// Parse the system parameters.
	if (!(len = element_set_str(g, cp, 10))) {
		syslog(LOG_ALERT, "%s: unable to parse system parameters",
				BLS_PAM_PUBFILE);
		goto error_set_g;
	}
	cp += len;
	// Parse the public key.
	if (!(len = element_set_str(public_key, cp, 10))) {
		syslog(LOG_ALERT, "%s: unable to parse public key",
				BLS_PAM_PUBFILE);
		goto error_set_public_key;
	}

	// Initialize the hashing context.
	gcry_md_hd_t hd;
	err = gcry_md_open(&hd, BLS_MD_ALGO, 0);
	if (err) {
		errno = gcry_err_code_to_errno(gcry_err_code(err));
		if (errno)
			syslog(LOG_CRIT, "%s", strerror(errno));
		retval = PAM_SYSTEM_ERR;
		goto error_open_hd;
	}

	// Read the message from file and compute the hash.
	stream = fopen(msgfile, "rb");
	if (!stream) {
		syslog(LOG_ALERT, "%s: %s", msgfile, strerror(errno));
		goto error_fopen_msgfile;
	}
	while (!feof(stream)) {
		n = fread(buf, 1, BLS_BUFSIZE, stream);
		if (n) {
			gcry_md_write(hd, buf, n);
		} else if (ferror(stream)) {
			fclose(stream);
			syslog(LOG_ALERT, "%s: unable to read entire file",
					msgfile);
			goto error_fread_msgfile;
		}
	}
	fclose(stream);

	// Generate the hash element.
	element_t h;
	element_init_G1(h, pairing);
	element_from_hash(h, gcry_md_read(hd, BLS_MD_ALGO),
			gcry_md_get_algo_dlen(BLS_MD_ALGO));

	// Allocate a storage buffer for the signature.
	len = pairing_length_in_bytes_x_only_G1(pairing);
	assert(len > 0);
	unsigned char *data = malloc(len);
	if (!data) {
		syslog(LOG_CRIT, "%s", strerror(errno));
		retval = PAM_SYSTEM_ERR;
		goto error_malloc_data;
	}
	memset(data, 0, len);

	// Read the signature from stdin.
	n = fread(buf, 1, BLS_BUFSIZE - 1, stdin);
	buf[n] = '\0';
	if (ferror(stdin) || !feof(stdin)) {
		syslog(LOG_ALERT, "unable to read signature from <stdin>");
		goto error_fread_stdin;
	}
	cp = buf;

	// Parse the Base32 signature (see RFC 4648).
	unsigned char *bp = data;
	i = 0;
	while (*cp && bp < data + len) {
		// Obtain the next 5 bits and ignore unknown characters.
		char c = *cp++;
		unsigned char b;
		if (isalpha((unsigned char)c))
			b = toupper((unsigned char)c) - 'A';
		else if (c >= '2' && c <= '7')
			b = 26 + c - '2';
		else
			continue;
		// Add the bits to the storage buffer.
		if (i <= 3) {
			*bp |= (b << (3 - i));
			if ((i += 5) == 8) {
				bp++;
				i = 0;
			}
		} else {
			*bp++ |= b >> (i - 3);
			if (bp < data + len)
				*bp |= b << (8 - (i -= 3));
		}
	}

	// Avoid a division-by-zero error.
	for (i = 0; i < len && !data[i]; i++);
	if (i == len) {
		syslog(LOG_ERR, "invalid signature");
		goto error_data;
	}

	// Obtain the x-coordinate of the signature.
	element_t sig;
	element_init_G1(sig, pairing);
	element_from_bytes_x_only(sig, data);

	// Apply the pairing to the signature and system parameters.
	element_t temp1;
	element_init_GT(temp1, pairing);
	pairing_apply(temp1, sig, g, pairing);

	// Apply the pairing to the hash and public key.
	element_t temp2;
	element_init_GT(temp2, pairing);
	pairing_apply(temp2, h, public_key, pairing);

	// Compare the pairing outputs. We might have to try twice, since we
	// only have the x-coordinate of the signature. See the Import/export
	// tutorial in the PBC Library Manual for details.
	retval = PAM_AUTH_ERR;
	if (element_cmp(temp1, temp2)) {
		element_invert(temp1, temp1);
		if (element_cmp(temp1, temp2)) {
			syslog(LOG_NOTICE, "signature check failed");
			goto error_sig;
		}
	}

	retval = PAM_SUCCESS;
error_sig:
	element_clear(temp2);
	element_clear(temp1);
	// Overwrite the signature with random data.
	element_random(sig);
	element_clear(sig);
error_data:
error_fread_stdin:
	// Clear the storage buffer for the signature.
	memset(data, 0, len);
	free(data);
error_malloc_data:
	element_clear(h);
error_fread_msgfile:
error_fopen_msgfile:
	gcry_md_close(hd);
error_open_hd:
error_set_public_key:
error_set_g:
error_fread_BLS_PAM_PUBFILE:
error_fopen_BLS_PAM_PUBFILE:
	element_clear(public_key);
	element_clear(g);
	pairing_clear(pairing);
error_init_pairing:
error_fread_BLS_PAM_PARFILE:
error_fopen_BLS_PAM_PARFILE:
	gcry_free(buf);
error_malloc_buf:
	gcry_control(GCRYCTL_TERM_SECMEM);
error_gcry_check_version:
	closelog();
	return retval;
}

