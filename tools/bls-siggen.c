/*!\file
 * This file contains the tool used to generate a BLS signature. See the
 * <a href="https://crypto.stanford.edu/pbc/manual/ch02s01.html">BLS signatures</a>
 * tutorial in the PBC Library Manual for details.
 *
 * \copyright 2017 Lely Industries N.V.
 *
 * \author J. S. Seldenthuis <jseldenthuis@lely.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tools.h"

#include <assert.h>
#include <errno.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gpgme.h>

#define HELP \
	"Usage: %s [options...] <parameter file> <private key file>\n" \
	"       [message file]\n" \
	"Options:\n" \
	"  -b, --binary          Write binary signature\n" \
	"  -h, --help            Display this information\n" \
	"  -o <file>, --output=<file>\n" \
	"                        Write the output to <file> instead of stdout\n" \
	"  -p, --password        Decrypt the private key with a password\n" \
	"  -q, --quiet           Do not print errors\n"

#define FLAG_BINARY	0x01
#define FLAG_HELP	0x02
#define FLAG_PASSWORD	0x04
#define FLAG_QUIET	0x08

int
main(int argc, char *argv[])
{
	const char *cmd = cmdname(argv[0]);

	int flags = 0;
	const char *outfile = NULL;
	const char *parfile = NULL;
	const char *keyfile = NULL;
	const char *msgfile = NULL;

	opterr = 0;
	optind = 1;
	int optpos = 0;
	while (optind < argc) {
		char *arg = argv[optind];
		if (*arg != '-') {
			optind++;
			switch (optpos++) {
			case 0:
				parfile = arg;
				break;
			case 1:
				keyfile = arg;
				break;
			case 2:
				msgfile = arg;
				break;
			default:
				fprintf(stderr, "%s: extra argument %s\n", cmd,
						arg);
				break;
			}
		} else if (*++arg == '-') {
			optind++;
			if (!*++arg)
				break;
			if (!strcmp(arg, "binary")) {
				flags |= FLAG_BINARY;
			} else if (!strcmp(arg, "help")) {
				flags |= FLAG_HELP;
			} else if (!strncmp(arg, "output=", 7)) {
				outfile = arg + 7;
			} else if (!strcmp(arg, "password")) {
				flags |= FLAG_PASSWORD;
			} else if (!strcmp(arg, "quiet")) {
				flags |= FLAG_QUIET;
			} else {
				fprintf(stderr, "%s: illegal option -- %s\n",
						cmd, arg);
			}
		} else {
			int c = getopt(argc, argv, ":bho:pq");
			if (c == -1)
				break;
			switch (c) {
			case ':':
				fprintf(stderr, "%s: option requires an argument -- %c\n",
						cmd, optopt);
				break;
			case '?':
				fprintf(stderr, "%s: illegal option -- %c\n",
						cmd, optopt);
				break;
			case 'b':
				flags |= FLAG_BINARY;
				break;
			case 'h':
				flags |= FLAG_HELP;
				break;
			case 'o':
				outfile = optarg;
				break;
			case 'p':
				flags |= FLAG_PASSWORD;
				break;
			case 'q':
				flags |= FLAG_QUIET;
				break;
			}
		}
	}
	for (char *arg = argv[optind]; optind < argc; arg = argv[++optind]) {
		switch (optpos++) {
		case 0:
			parfile = arg;
			break;
		case 1:
			keyfile = arg;
			break;
		case 2:
			msgfile = arg;
			break;
		default:
			fprintf(stderr, "%s: extra argument %s\n", cmd, arg);
			break;
		}
	}

	if (flags & FLAG_HELP) {
		fprintf(stderr, HELP, cmd);
		return EXIT_SUCCESS;
	}

	if (optpos < 1 || !parfile) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: no parameter file specified\n",
					cmd);
		return EXIT_FAILURE;
	}

	if (optpos < 2 || !keyfile) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: no private key file specified\n",
					cmd);
		return EXIT_FAILURE;
	}

	int retval = EXIT_FAILURE;
	gpg_error_t err;
	FILE *stream;
	size_t n;
	char *cp;
	int len;

	// Initialize GPGME, if necessary.
	if (flags & FLAG_PASSWORD) {
		setlocale(LC_ALL, "");
		if (!gpgme_check_version(NULL)) {
			fprintf(stderr, "%s: GPGME version mismatch\n", cmd);
			goto error_gpgme_check_version;
		}
		err = gpgme_set_locale(NULL, LC_CTYPE,
				setlocale(LC_CTYPE, NULL));
		assert(!err);
		err = gpgme_set_locale(NULL, LC_MESSAGES,
				setlocale(LC_MESSAGES, NULL));
		assert(!err);
		err = gpgme_engine_check_version(GPGME_PROTOCOL_OpenPGP);
		assert(!err);
	}

	// Initialize Libgcrypt.
	if (!gcry_check_version(GCRYPT_VERSION)) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: libgcrypt version mismatch\n",
					cmd);
		goto error_gcry_check_version;
	}
	err = gcry_control(GCRYCTL_INIT_SECMEM, BLS_BUFSIZE, 0);
	assert(!err);
	err = gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
	assert(!err);

	// Allocate a secure memory buffer for reading files.
	char *buf = gcry_malloc_secure(BLS_BUFSIZE);
	if (!buf) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to allocate secure memory\n",
					cmd);
		goto error_malloc_buf;
	}

	pairing_t pairing;
	// Read the pairing parameters from file.
	stream = fopen(parfile, "r");
	if (!stream) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: %s\n", parfile, strerror(errno));
		goto error_fopen_parfile;
	}
	n = fread(buf, 1, BLS_BUFSIZE - 1, stream);
	buf[n] = '\0';
	if (ferror(stream) || !feof(stream)) {
		fclose(stream);
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to read entire file\n",
					parfile);
		goto error_fread_parfile;
	}
	fclose(stream);
	// Parse the pairing parameters.
	if (pairing_init_set_buf(pairing, buf, n)) {
		fprintf(stderr, "%s: unable to parse pairing parameters\n",
				parfile);
		goto error_init_pairing;
	}

	// Initialize the system parameters.
	element_t g;
	element_init_G2(g, pairing);

	// Initialize the private key.
	element_t secret_key;
	element_init_Zr(secret_key, pairing);

	// Read the private key from file.
	stream = fopen(keyfile, "r");
	if (!stream) {
		fprintf(stderr, "%s: %s\n", keyfile, strerror(errno));
		goto error_fopen_keyfile;
	}
	n = fread(buf, 1, BLS_BUFSIZE - 1, stream);
	if (ferror(stream) || !feof(stream)) {
		fclose(stream);
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to read entire file\n",
					keyfile);
		goto error_fread_keyfile;
	}
	fclose(stream);
	// Decrypt the private key, if necessary.
	if (flags & FLAG_PASSWORD) {
		gpgme_data_t in;
		err = gpgme_data_new_from_mem(&in, buf, n, 0);
		if (err)
			goto error_new_in;

		// Create an OpenPGP context.
		gpgme_ctx_t ctx;
		err = gpgme_new(&ctx);
		if (err)
			goto error_new_ctx;
		err = gpgme_set_protocol(ctx, GPGME_PROTOCOL_OpenPGP);
		if (err)
			goto error_gpgme_set_protocol;

		gpgme_data_t out;
		err = gpgme_data_new(&out);
		if (err)
			goto error_new_out;

		err = gpgme_op_decrypt(ctx, in, out);
		if (err) {
			gpgme_data_release(out);
			goto error_op_decrypt;
		}

		char *tmp = gpgme_data_release_and_get_mem(out, &n);
		if (tmp) {
			memcpy(buf, tmp, n);
			memset(tmp, 0, n);
			gpgme_free(tmp);
		} else {
			n = 0;
			err = gpgme_err_code_from_errno(errno);
		}

	error_op_decrypt:
	error_new_out:
	error_gpgme_set_protocol:
		gpgme_release(ctx);
	error_new_ctx:
		gpgme_data_release(in);
	error_new_in:
		if (err) {
			errno = gpgme_err_code_to_errno(gpgme_err_code(err));
			if (errno)
				fprintf(stderr, "%s: %s\n", cmd,
						strerror(errno));
			goto error_decrypt;
		}
	}
	buf[n] = '\0';
	cp = buf;
	// Parse the system parameters.
	if (!(len = element_set_str(g, cp, 10))) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to parse system parameters\n",
					keyfile);
		goto error_set_g;
	}
	cp += len;
	// Parse the private key.
	if (!(len = element_set_str(secret_key, cp, 10))) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: unable to parse private key\n",
					keyfile);
		goto error_set_secret_key;
	}
	// The private key cannot be 0, but it might be if a public key was
	// provided instead.
	if (element_is0(secret_key)) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: invalid private key\n", keyfile);
		goto error_secret_key_is0;
	}

	// Initialize the hashing context.
	gcry_md_hd_t hd;
	err = gcry_md_open(&hd, BLS_MD_ALGO, 0);
	if (err) {
		errno = gcry_err_code_to_errno(gcry_err_code(err));
		if (errno && !(flags & FLAG_QUIET))
			fprintf(stderr, "%s: %s\n", cmd, strerror(errno));
		goto error_open_hd;
	}

	// Read the message and compute the hash.
	stream = msgfile ? fopen(msgfile, "rb") : stdin;
	if (!stream) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: %s\n", msgfile, strerror(errno));
		goto error_fopen_msgfile;
	}
	while (!feof(stream)) {
		n = fread(buf, 1, BLS_BUFSIZE, stream);
		if (n) {
			gcry_md_write(hd, buf, n);
		} else if (ferror(stream)) {
			if (msgfile)
				fclose(stream);
			if (!(flags & FLAG_QUIET))
				fprintf(stderr, "%s: read error\n",
						msgfile ? msgfile : "<stdin>");
			goto error_fread_msg;
		}
	}
	if (msgfile)
		fclose(stream);

	// Generate the hash element.
	element_t h;
	element_init_G1(h, pairing);
	element_from_hash(h, gcry_md_read(hd, BLS_MD_ALGO),
			gcry_md_get_algo_dlen(BLS_MD_ALGO));

	// Generate the signature.
	element_t sig;
	element_init_G1(sig, pairing);
	element_pow_zn(sig, h, secret_key);

	// Allocate a storage buffer for the signature.
	len = pairing_length_in_bytes_x_only_G1(pairing);
	assert(len > 0);
	unsigned char *data = malloc(len);
	if (!data) {
		if (!(flags & FLAG_QUIET))
			fprintf(stderr, "%s: %s\n", cmd, strerror(errno));
		goto error_malloc_data;
	}
	memset(data, 0, len);

	// Store the x-coordinate of the signature.
	element_to_bytes_x_only(data, sig);

	stream = stdout;
	if (outfile) {
		stream = fopen(outfile, (flags & FLAG_BINARY) ? "wb" : "w");
		if (!stream) {
			if (!(flags & FLAG_QUIET))
				fprintf(stderr, "%s: %s\n", outfile,
						strerror(errno));
			goto error_fopen_outfile;
		}
	}

	if (flags & FLAG_BINARY) {
		// Write the binary signature.
		if (fwrite(data, 1, len, stream) != (size_t)len) {
			if (!(flags & FLAG_QUIET))
				fprintf(stderr, "%s: %s\n",
						outfile ? outfile : "<stdin>",
						strerror(errno));
			if (outfile)
				fclose(stream);
			goto error_fwrite_outfile;
		}
	} else {
		// Print the signature in Base32 (see RFC 4648).
		const char base32[32] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
		unsigned char *bp = data;
		int i = 8;
		n = 0;
		while (bp < data + len) {
			// Print the next 5 bits.
			unsigned char b;
			if (i >= 5) {
				b = *bp >> (i - 5);
				if (!(i -= 5)) {
					bp++;
					i = 8;
				}
			} else {
				b = *bp++ << (5 - i);
				if (bp < data + len)
					b |= *bp >> (i += 3);
			}
			fputc(base32[b & 0x1f], stream);
			// Separate groups of four characters with dashes.
			if (!(++n % 4) && bp < data + len)
				fputc('-', stream);
		}
		fputc('\n', stream);
	}

	if (outfile)
		fclose(stream);

	retval = EXIT_SUCCESS;
error_fwrite_outfile:
error_fopen_outfile:
	free(data);
error_malloc_data:
	element_clear(sig);
	element_clear(h);
error_fread_msg:
error_fopen_msgfile:
	gcry_md_close(hd);
error_open_hd:
error_secret_key_is0:
error_set_secret_key:
error_set_g:
error_decrypt:
error_fread_keyfile:
error_fopen_keyfile:
	// Overwrite the secret key with random data.
	element_random(secret_key);
	element_clear(secret_key);
	element_clear(g);
	pairing_clear(pairing);
error_init_pairing:
error_fread_parfile:
error_fopen_parfile:
	gcry_free(buf);
error_malloc_buf:
	gcry_control(GCRYCTL_TERM_SECMEM);
error_gcry_check_version:
error_gpgme_check_version:
	return retval;
}

