BLS signature tools and PAM module
==================================

Introduction
------------

This project contains tools to generate and verify Boneh-Lynn-Shacham ([BLS])
signatures, as well as a pluggable authentication module ([PAM]) for Linux based
on these signatures.

BLS signatures are pairing-based cryptographic signatures optimized for size.
This makes it feasible for them to be used as passwords for authentication
(signatures produced by the RSA Full-Domain Hash scheme are far too large). The
tools in this project use the 160-bit type F pairing family, which produces the
shortest signatures but is also the slowest. The signatures are generated from a
[SHA-224] hash of the message and encoded with [Base32].

BLS signatures can be used for user authentication on systems for which it is
infeasible or undesirable to configure a password (such as a system
administrator account on mass-produced IoT devices). Each system is equipped
with a public key and a user- and/or device-specific message (such as a
combination of user name and MAC address or serial number). This message can
also be public, but it must be read-only. To gain access, a user needs to enter
the BLS signature of the message (generated with the private key), which the
system can then verify using the public key. Since the system does not contain
the private key, gaining even administrator access to one device does not
compromise the security of another.

Download
--------

The project is hosted on [GitLab]. You can clone the repository with

    $ git clone https://gitlab.com/lely_industries/bls.git

or download the latest release
([v1.2.1](https://gitlab.com/lely_industries/bls/tags/v1.2.1)).

Binary packages for Ubuntu 14.04 LTS (Trusty Tahr) and 16.04 LTS (Xenial Xerus)
can be found at https://launchpad.net/~lely/+archive/ubuntu/ppa.

Build and install
-----------------

This project uses the GNU Build System (`configure`, `make`, `make install`).
The release archive includes the build system, but if you want to build a
checkout of the repository, you need to install the autotools (autoconf,
automake and libtool). The build system can then be generated by running

    $ autoreconf -i

in the root directory of the repository.

The BLS signature tools depend on [Libgcrypt] and the [PBC Library]; the PAM
module depends on [Linux-PAM].

### Debian packages

For Debian-based Linux distributions, the preferred installation method is to
generate and install Debian packages. Install the Debian package build system
with

    $ sudo apt-get install cdbs devscripts

and the build dependencies with

    $ sudo apt-get install libgcrypt20-dev libpam0g-dev libpbc-dev

The last package is not part of the Debian repositories. It can be obtained from
the lely PPA, which can be added with

    $ sudo add-apt-repository ppa:lely/ppa
    $ sudo apt-get update

Checkout the debian branch. The packages can now be built by running

    $ debuild -uc -us -b

from the root directory of the project. Once the packages are generated, you can
clean up the project directory with

    $ debuild clean

debuild creates the packages in the parent directory of the project. Install
them with

    $ cd ..
    $ sudo dpkg -i *.deb

Node that the libpam-bls package automatically enables the pam_bls
authentication module with pam-auth-update.

### configure, make, make install

It is also possible to build and install the library by hand. First, configure
the build system by running

    $ ./configure

from the root directory of the project.

The `configure` script supports many options. The full list can be shown with

    $ ./configure --help

The PAM module will be installed under `$(libdir)/security`. It may be necessary
to provide the `--libdir=DIR` option to make sure it gets installed in the right
location.

Once the build system is configured, the tools and the PAM module can be built
with

    $ make

and installed by running

    # make install

as root.

Usage
-----

For signing and verification we need a private key and its corresponding public
key. Such a key pair can be generated (from `/dev/urandom`) with

    $ bls-keygen

Use the `-p`/`--password` option to encrypt the private key.

This command produces three files in the current directory:
  * `bls.par`, which contains the 160-bit type F pairing parameters.
  * `bls.key` or `bls.key.gpg`, which contains the private key. This file should
    be kept secret, especially if it is not encrypted.
  * `bls.pub`, which contains the public key. This file can be distributed
    (together with `bls.par`).

### Signing and verification

Signatures can be generated with bls-siggen using the private key (and the
pairing parameters). For example,

    $ echo "Hello, world!" > test.msg
    $ bls-siggen bls.par bls.key test.msg
    BT2N-X5ZR-THBE-NYNZ-WIGB-QCRT-HZYI-G3P3

The actual value of the signature depends, of course, on the value of the
private key.

Signatures can be verified with bls-sigchk using the public key (and, again, the
pairing parameters). For example,

    $ bls-sigchk bls.par bls.pub test.msg
    > BT2N-X5ZR-THBE-NYNZ-WIGB-QCRT-HZYI-G3P3
    $ echo $?
    0

A return value of 0 means the signature is valid. An invalid signature results
in a return value of 1, as well as a warning:

    $ bls-sigchk bls.par bls.pub test.msg
    > BT2N-X5ZR-THBE-NYNZ-WIGB-QCRT-HZYI-G3P4
    bls-sigchk: signature check failed
    $ echo $?
    1

The warning can be suppressed with the `-q`/`--quiet` option. bls-siggen and
bls-sigchk print and parse Base32-encoded signatures by default. To write or
read 20-byte binary signatures, use the `-b`/`--binary` option. The
`-h`/`--help` option gives an overview of all options.

### PAM authentication

The BLS PAM module requires the pairing parameters and the public key to be
available under `/etc/bls/pam.par` and `/etc/bls/pam.pub`, respectively, as well
as a message file for each user at `/etc/bls/$USER.msg`. It is recommended that
the message file uniquely identifies both the user and the device. For example,

    $ cat /etc/bls/root.msg
    root@01:23:45:67:89:ab

which combines the user name and MAC address.

The first line of the message file is displayed by the PAM module (unless the
PAM_SILENT flag is set, as is the case with sudo). To authenticate a user, enter
the signature for the user's message (as produced by bls-siggen) at the prompt.

To use BLS signatures with OpenSSH, make sure that both
**ChallengeResponseAuthentication** and **UsePAM** are set to `yes` in
`/etc/ssh/sshd_config`.

Licensing
---------

Copyright 2017 [Lely Industries N.V.]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[Base32]: https://tools.ietf.org/html/rfc4648
[BLS]: https://en.wikipedia.org/wiki/Boneh%E2%80%93Lynn%E2%80%93Shacham
[GitLab]: https://gitlab.com/lely_industries/bls
[Lely Industries N.V.]: http://www.lely.com
[Libgcrypt]: https://www.gnu.org/software/libgcrypt/
[Linux-PAM]: http://www.linux-pam.org/
[PAM]: https://en.wikipedia.org/wiki/Pluggable_authentication_module
[PBC Library]: https://crypto.stanford.edu/pbc/
[SHA-224]: https://en.wikipedia.org/wiki/SHA-2

